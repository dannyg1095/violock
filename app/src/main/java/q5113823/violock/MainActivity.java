package q5113823.violock;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.hardware.camera2.*;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.view.accessibility.AccessibilityEvent.TYPE_VIEW_CLICKED;

/**
 * Created by Daniel on 19/02/2020.
 */

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private ListView listView;
    private AppDataAdapter appDataAdapter;
    private ProgressBar progressBar;

    private Handler mHandler = new Handler();

    protected static ArrayList<String> lockedApps;

    private ArrayList<AppData> appData = new ArrayList<>();

    private static long timeLastAuthorised = System.currentTimeMillis();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        final Context context = this;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppData ad = (AppData) parent.getItemAtPosition(position);
                appDataAdapter.updateElementPos(ad);

                if(ad.isLocked() && !lockedApps.contains(ad.getPackageName())) {
                    lockedApps.add(ad.getPackageName());
                    saveLockedApps();
                } else if(!ad.isLocked() && lockedApps.contains(ad.getPackageName())) {
                    lockedApps.remove(ad.getPackageName());
                    saveLockedApps();
                }

                appDataAdapter.notifyDataSetChanged();

                Toast.makeText(context, ad.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        progressBar = findViewById(R.id.progressBar);

        final PackageManager pm = getPackageManager();
        //get a list of installed apps.
        final List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        //packageNames = new ArrayList<>();

        progressBar.setVisibility(View.VISIBLE);

        lockedApps = FileManager.getLockedApps(this);

        Log.d(TAG, "lockedApps: "+lockedApps.size());

        for(String app : lockedApps) {
            Log.d(TAG, "lockedApps: "+app);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<AppData> lockedAppsLocal = new ArrayList<>();
                appDataAdapter = new AppDataAdapter(getApplicationContext(), appData);
                for (ApplicationInfo packageInfo : packages) {
                    //appNames.add((String) pm.getApplicationLabel(packageInfo));
                    try {
                        AppData ad = new AppData((String) pm.getApplicationLabel(packageInfo), packageInfo.packageName, pm.getApplicationIcon(packageInfo.packageName));

                        System.out.println(ad.getPackageName());
                        if(lockedApps.contains(packageInfo.packageName)) {
                            ad.setLocked(true);
                            lockedAppsLocal.add(ad);
                        } else {
                            ad.setLocked(false);
                            appData.add(ad);
                        }

                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                appData.addAll(0, lockedAppsLocal);

                // the getLaunchIntentForPackage returns an intent that you can use with startActivity()

                appDataAdapter.notifyDataSetChanged();

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        listView.setAdapter(appDataAdapter);
                    }
                });
            }
        }).start();

        //AppListenerIntentService alis = new AppListenerIntentService();
        scheduleJob();
    }

    private final String FILE_NAME = "data.txt";

    @Override
    protected void onResume() {
        super.onResume();
        //setContentView(R.layout.activity_main);

        Log.d(TAG, "onResume: "+appData.size());

        long currentTime = System.currentTimeMillis();
        if(currentTime - timeLastAuthorised > 5000) {
            Log.d(TAG, "currentTime: "+currentTime);
            Log.d(TAG, "timeLastAuthorised: "+timeLastAuthorised);
            Log.d(TAG, "currentTime - timeLastAuthorised: "+(currentTime-timeLastAuthorised));

            timeLastAuthorised = currentTime;

            //Intent i = new Intent(getBaseContext(), StartActivity.class);
            //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            //startActivityForResult(i, 2);

            File file = new File(getFilesDir(), FILE_NAME);

            try {
                String[] auth = FileManager.openFile(this);

                Intent i;

                if (auth[0] == null) { //if file doesn't exist
                    file.createNewFile();
                    finish();
                } else if (auth[0].equals("pin")) {
                    i = new Intent(getBaseContext(), PinActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(i);
                } else if (auth[0].equals("password")) {
                    i = new Intent(getBaseContext(), PasswordActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(i);
                } else if (auth[0].equals("pattern")) {
                    i = new Intent(getBaseContext(), PatternLockActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(i);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void scheduleJob() {
        ComponentName componentName = new ComponentName(this, AppLockJobService.class);
        JobInfo info = new JobInfo.Builder(123, componentName)
                .setPersisted(true)
                .setMinimumLatency(1)
                .setOverrideDeadline(1)
                .build();

        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);

        int resultCode = scheduler.schedule(info);
        if(resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "scheduleJob: Job Scheduled");
        } else if (resultCode == JobScheduler.RESULT_FAILURE){
            Log.d(TAG, "scheduleJob: Job Scheduling Failed");
        }
    }

    public void cancelJob() {
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(123);
        Log.d(TAG, "cancelJob: Job Cancelled");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settingsmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.settings) {
            if(AppLockJobService.IsSettingsLocked()) {
                AppLockJobService.UnlockSettings();
                Toast.makeText(this, "settings unlocked", Toast.LENGTH_SHORT).show();
            } else {
                AppLockJobService.LockSettings();
                Toast.makeText(this, "settings locked", Toast.LENGTH_SHORT).show();
            }
            AppLockJobService.saveSettings(this);
        } else if (id == R.id.phone) {
            if(AppLockJobService.IsPhoneLocked()) {
                AppLockJobService.UnlockPhone();
                Toast.makeText(this, "phone unlocked", Toast.LENGTH_SHORT).show();
            } else {
                AppLockJobService.LockPhone();
                Toast.makeText(this, "phone locked", Toast.LENGTH_SHORT).show();
            }
            AppLockJobService.saveSettings(this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveLockedApps() {
        FileManager.saveLockedApps(this, lockedApps);
    }
}
