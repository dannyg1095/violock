package q5113823.violock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreatePinActivity extends AppCompatActivity {
    private Pinview mPinView;
    private String firstPin = "";
    private int iterate = 1;

    private TextView mTextView; //please insert pin/password/pattern lock

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pin);

        mTextView = findViewById(R.id.textView);

        mPinView = findViewById(R.id.pinView);
        mPinView.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if(iterate == 1) {
                    firstPin = mPinView.getValue();
                    mPinView.setValue("");
                    mTextView.setText("Confirm PIN");
                    iterate--;
                } else if(iterate == 0) {
                    if(firstPin.equals(mPinView.getValue())) {
                        Intent i = new Intent(getBaseContext(), PinActivity.class);
                        i.putExtra("toMain", true);
                        //i.putExtra("pin", firstPin);
                        saveFile("pin" , firstPin);
                        setResult(RESULT_OK);
                        startActivityForResult(i, 1);
                        finish();
                    } else {
                        Toast.makeText(CreatePinActivity.this, "PIN mismatch. Try again.", Toast.LENGTH_SHORT).show();
                        mTextView.setText("Enter New PIN");
                        mPinView.setValue("");
                        iterate++;
                    }
                }
                /*if (pin.equals(mPinView.getValue())) {
                    finish();
                } else {
                    retries--;
                    if(retries > 0) {
                        Toast.makeText(CreatePinActivity.this, "Retries Left: "+retries, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CreatePinActivity.this, "TOO MANY RETRIES", Toast.LENGTH_SHORT).show();
                    }
                }*/
            }
        });
    }

    private void saveFile(String auth, String string) {
        FileManager.saveFile(this, auth, string);

        /*
        FileOutputStream fos = null;

        try {
            fos = openFileOutput("data.txt", MODE_PRIVATE);
            auth += "\n"+string;
            fos.write(auth.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
    }
}
