package q5113823.violock;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

/**
 * Created by Daniel on 20/02/2020.
 */

public class AppListenerIntentService extends IntentService {
    private static final String TAG = "AppListenerIntentServic";
    
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public AppListenerIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        /*
        ActivityManager activityManager = (ActivityManager) getBaseContext().getSystemService(getBaseContext().ACTIVITY_SERVICE);
        List<ActivityManager.AppTask> tasks = activityManager.getAppTasks();

        for (ActivityManager.AppTask task : tasks) {
            Log.d(TAG, "stackId: " + task.getTaskInfo().baseActivity.getPackageName());
        }*/
        Log.d(TAG, "onHandleIntent: "+intent.getPackage());
    }
}
