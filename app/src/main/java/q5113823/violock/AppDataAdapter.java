package q5113823.violock;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 20/02/2020.
 */

public class AppDataAdapter extends ArrayAdapter<AppData> {

    private Context _context;
    private List<AppData> _appData = new ArrayList<>();
    private List<AppData> _lockedApps = new ArrayList<>();

    public AppDataAdapter(@NonNull Context context, ArrayList<AppData> appData) {
        super(context, 0, appData);

        _context = context;
        _appData = appData;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null) {
            listItem = LayoutInflater.from(_context).inflate(R.layout.appdata_list_item, parent, false);
        }

        AppData currentAppData = _appData.get(position);

        ImageView icon = listItem.findViewById(R.id.appIcon);
        icon.setImageDrawable(currentAppData.getIcon());
        icon.setTag(currentAppData.getPackageName());

        TextView name = listItem.findViewById(R.id.appName);
        name.setText(currentAppData.getName());

        CheckBox cb = listItem.findViewById(R.id.locked);
        cb.setChecked(currentAppData.isLocked());

        return listItem;
    }

    public void updateElementPos(AppData element) {
        if(element.isLocked()) {
            _lockedApps.remove(element);
            element.setLocked(false);
        } else {
            _appData.remove(element);
            _appData.add(0, element);
            _lockedApps.add(0, element);
            element.setLocked(true);
        }

        notifyDataSetChanged();
    }
}
