package q5113823.violock;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreatePasswordActivity extends AppCompatActivity {
    private TextView pTextView;
    private String firstPassword = "";
    private Button okButton;
    private int i = 1;

    private TextView mTextView; //please insert pin/password/pattern lock

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);

        mTextView = findViewById(R.id.textView);
        pTextView = findViewById(R.id.editText);

        // ----- password -----
        okButton = findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i == 1) {
                    firstPassword = pTextView.getText().toString();
                    pTextView.setText("");
                    mTextView.setText("Confirm Password");
                    i--;
                } else if(i == 0) {
                    if(firstPassword.equals(pTextView.getText().toString())) {
                        Intent i = new Intent(getBaseContext(), PasswordActivity.class);
                        i.putExtra("toMain", true);
                        saveFile("password", firstPassword);
                        setResult(RESULT_OK);
                        startActivityForResult(i, 1);
                        finish();
                    } else {
                        Toast.makeText(CreatePasswordActivity.this, "password mismatch. Try again.", Toast.LENGTH_SHORT).show();
                        mTextView.setText("Enter New Password");
                        pTextView.setText("");
                        i++;
                    }
                }
                Toast.makeText(CreatePasswordActivity.this, "Button Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        // ----- password -----

        /*
        // ----- pin -----
        mPinView = findViewById(R.id.pinView);
        mPinView.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if(pinIterate == 1) {
                    firstPin = mPinView.getValue();
                    mPinView.setValue("");
                    mTextView.setText("Confirm PIN");
                    pinIterate--;
                } else if(pinIterate == 0) {
                    if(firstPin.equals(mPinView.getValue())) {
                        Intent i = new Intent(getBaseContext(), PinActivity.class);
                        //i.putExtra("pin", firstPin);
                        saveFile("pin" , firstPin);
                        setResult(RESULT_OK);
                        startActivityForResult(i, 1);
                        finish();
                    } else {
                        Toast.makeText(CreatePasswordActivity.this, "PIN mismatch. Try again.", Toast.LENGTH_SHORT).show();
                        mTextView.setText("Enter New PIN");
                        mPinView.setValue("");
                        pinIterate++;
                    }
                }
            }
        });*/
        // ----- pin -----
    }

    private void saveFile(String auth, String string) {
        FileManager.saveFile(this, auth, string);
        /*FileOutputStream fos = null;

        try {
            fos = openFileOutput("data.txt", MODE_PRIVATE);
            auth += "\n"+string;
            fos.write(auth.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
    }
}
