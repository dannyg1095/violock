package q5113823.violock;

import android.graphics.drawable.Drawable;

/**
 * Created by Daniel on 20/02/2020.
 */

public class AppData {
    private String name;
    private String packageName;
    private boolean locked;
    private Drawable icon;

    AppData(String name, String packageName, Drawable icon, boolean locked) {
        this.name = name;
        this.packageName = packageName;
        this.icon = icon;
        this.locked = locked;
    }

    AppData(String name, String packageName, Drawable icon) {
        this.name = name;
        this.packageName = packageName;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
