package q5113823.violock;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class PasswordActivity extends AppCompatActivity {
    private TextView mEditText;
    private Button okButton;
    private int retries = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        Bundle bundle = getIntent().getExtras();
        final Class previousActivity = bundle != null ? (Class) bundle.get("previousActivity") : null;
        final boolean toMain = bundle != null ? (boolean) bundle.get("toMain") : false;
        final String[] password = openFile();

        if(!password[0].equals("password")) {
            finish(); // no point in continuing if there isn't even a password
            return; // not sure if needed with "finish()"?
        }

        mEditText = findViewById(R.id.password);

        okButton = findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password[1].equals(mEditText.getText().toString())) {
                    //Intent i = new Intent(getBaseContext(), MainActivity.class); //Define new intent here
                    Intent i;
                    //i.putExtra("pin", pin);
                    //setResult(RESULT_OK, i);
                    //startActivity(i); //Start new activity here
                    if(previousActivity != null) {
                        i = new Intent(getBaseContext(), previousActivity);
                        Toast.makeText(PasswordActivity.this, "to previousActivity", Toast.LENGTH_SHORT);
                        setResult(RESULT_OK, i);
                        startActivity(i);
                    } else if(toMain) {
                        Toast.makeText(PasswordActivity.this, "to main activity", Toast.LENGTH_SHORT);
                        i = new Intent(getBaseContext(), MainActivity.class); //Define new intent here
                        setResult(RESULT_OK, i);
                        startActivity(i);
                    } else {
                        moveTaskToBack(true);
                    }
                    finish();
                } else {
                    retries--;
                    if(retries > 0) {
                        Toast.makeText(PasswordActivity.this, "Retries Left: "+retries, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PasswordActivity.this, "TOO MANY RETRIES", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private String[] openFile() {
        return FileManager.openFile(this);
    }
}
