package q5113823.violock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChooseAuthenticationActivity extends AppCompatActivity {

    private Button passwordButton;
    private Button pinButton;
    private Button patternButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_authentication);

        passwordButton = findViewById(R.id.passwordButton);
        pinButton = findViewById(R.id.pinButton);
        patternButton = findViewById(R.id.patternButton);

        passwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), CreatePasswordActivity.class);
                startActivity(i);
                finish();
            }
        });

        pinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), CreatePinActivity.class);
                startActivity(i);
                finish();
            }
        });

        patternButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), CreatePatternLockActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
