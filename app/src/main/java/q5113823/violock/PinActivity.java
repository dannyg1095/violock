package q5113823.violock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinActivity extends AppCompatActivity {
    private Pinview mPinView;
    private int retries = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        Bundle bundle = getIntent().getExtras();
        final Class previousActivity = bundle != null ? (Class) bundle.get("previousActivity") : null;
        final boolean toMain = bundle != null ? (boolean) bundle.get("toMain") : false;
        final String[] pin = openFile();

        if(!pin[0].equals("pin")) {
            finish(); // no point in continuing if there isn't even a pin
            return; // not sure if needed with "finish()"?
        }

        mPinView = findViewById(R.id.pinView);
        mPinView.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if (pin[1].equals(mPinView.getValue())) {
                    //Intent i = new Intent(getBaseContext(), MainActivity.class); //Define new intent here
                    Intent i;
                    //i.putExtra("pin", pin);
                    //setResult(RESULT_OK, i);
                    //startActivity(i); //Start new activity here
                    if(previousActivity != null) {
                        i = new Intent(getBaseContext(), previousActivity);
                        Toast.makeText(PinActivity.this, "to previousActivity", Toast.LENGTH_SHORT);
                        setResult(RESULT_OK, i);
                        startActivity(i);
                    } else if(toMain) {
                        Toast.makeText(PinActivity.this, "to main activity", Toast.LENGTH_SHORT);
                        i = new Intent(getBaseContext(), MainActivity.class); //Define new intent here
                        setResult(RESULT_OK, i);
                        startActivity(i);
                    } else {
                        moveTaskToBack(true);
                    }
                    finish();
                } else {
                    retries--;
                    if(retries > 0) {
                        Toast.makeText(PinActivity.this, "Retries Left: "+retries, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PinActivity.this, "TOO MANY RETRIES", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private String[] openFile() {
        return FileManager.openFile(this);
    }
}
