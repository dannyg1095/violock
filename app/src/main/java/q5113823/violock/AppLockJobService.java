package q5113823.violock;

import android.Manifest;
import android.app.ActivityManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Daniel on 19/02/2020.
 */

public class AppLockJobService extends JobService {
    private static final String TAG = "AppLockJobService";
    private boolean jobCancelled;
    private String visited = "";

    private PowerManager pm;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "onStartJob: Job Started");

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);

        jobCancelled = false;

        final String[] auth = FileManager.openFile(this);

        final String[] settings = FileManager.getAppLockSettings(this);

        if(settings[0].equals("true")) SETTINGS_LOCKED = true;
        if(settings[1].equals("true")) PHONE_LOCKED = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                if(jobCancelled) return;

                while(true) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (pm.isInteractive()) {

                        UsageStatsManager usm = (UsageStatsManager) getApplicationContext().getSystemService(Context.USAGE_STATS_SERVICE);
                        long currentTime = System.currentTimeMillis();
                        List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, currentTime - 1000, currentTime);
                        UsageEvents appListEvents = usm.queryEvents(currentTime - 1000, currentTime);

                        if (appList != null && appList.size() > 0) {
                            SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                            for (UsageStats usageStats : appList) {
//                            Log.d(TAG, "usage stats executed:   " +usageStats.getPackageName() + "\n" +
//                                    "last time used:         " + usageStats.getLastTimeUsed() + "\n" +
//                                    "current time:           " + currentTime);

//                            if(MainActivity.packageNames.contains(usageStats.getPackageName())) {
//                                if(appListEvents.hasNextEvent()) {
//                                    UsageEvents.Event event = new UsageEvents.Event();
//                                    appListEvents.getNextEvent(event);
//                                    if(event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
//                                        Log.d(TAG, "usage stats executed:   " +usageStats.getPackageName() + "\n" +
//                                                "last time used:         " + usageStats.getLastTimeUsed() + "\n" +
//                                                "current time:           " + currentTime);
//                                        Log.d(TAG, "APP FOUND");
//                                    }
//                                } else {
//                                    //Log.d(TAG, "applist event: no event found");
//                                }
//                            }

                                //if(usageStats.getLastTimeUsed() > currentTime-100) {
                                //    Log.d(TAG, "time check: app last used within 100 milliseconds");
                                //    return;
                                //}
                                mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                            }
                            if (mySortedMap != null && !mySortedMap.isEmpty() && appListEvents.hasNextEvent()) {
                                String currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                                UsageEvents.Event event = new UsageEvents.Event();
                                appListEvents.getNextEvent(event);
                                int eventType = event.getEventType();
                                String eventPackage = event.getPackageName();

                                if ((DEVICE_SLEEPING && PHONE_LOCKED) || (MainActivity.lockedApps.contains(currentApp) || (currentApp.equals(SETTINGS_STRING) && SETTINGS_LOCKED)) && !visited.equals(currentApp) && eventPackage.equals(currentApp) && eventType == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                                    if (DEVICE_SLEEPING) {
                                        DEVICE_SLEEPING = false;
                                    } else {
                                        visited = currentApp;
                                    }

                                    Intent i;
                                    if (auth[0].equals("pin")) {
                                        i = new Intent(getBaseContext(), PinActivity.class);
                                    } else if (auth[0].equals("password")) {
                                        i = new Intent(getBaseContext(), PasswordActivity.class);
                                    } else if (auth[0].equals("pattern")) {
                                        i = new Intent(getBaseContext(), PatternLockActivity.class);
                                    } else {
                                        i = new Intent(getBaseContext(), PatternLockActivity.class); //just to shut AS up
                                    }
                                    startActivity(i);

                                } else if (eventPackage.equals(currentApp) && visited.equals(currentApp) && appListEvents.hasNextEvent()) {
                                    Log.d(TAG, "usage stats currentApp: " + currentApp);
                                    if (eventType == UsageEvents.Event.MOVE_TO_BACKGROUND) {
                                        visited = "";
                                    }

                                }
                            } else {

                            }
                        } else {
                            //Log.d(TAG, "usage stats: appList null or empty "+appList.size());
                        }


                        //ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(getApplication().ACTIVITY_SERVICE);
                        //List<ActivityManager.AppTask> AppTasks = activityManager.getAppTasks();
                        //List<ActivityManager.RunningAppProcessInfo> ProcInfos = activityManager.getRunningAppProcesses();

                        //String name = activityManager.getRunningAppProcesses().get(0).processName;

                        //for(ActivityManager.RunningAppProcessInfo ProcInfo : ProcInfos) {
                        //Log.d(TAG, "foreground process " + i + ": " + activityManager.getRunningAppProcesses().get(i).processName);
                        //Log.d(TAG, "foreground process: " + ProcInfo);
                        //}

                        //for (ActivityManager.AppTask task : tasks) {
                        //    Log.d(TAG, "stackId: " + task.getTaskInfo());
                        //}
                    } else if(!DEVICE_SLEEPING) DEVICE_SLEEPING = true;
                }

                /* Old method
                try
                {
                    Process mLogcatProc = null;
                    BufferedReader reader = null;
                    mLogcatProc = Runtime.getRuntime().exec(new String[]{"logcat", "-d"});

                    reader = new BufferedReader(new InputStreamReader(mLogcatProc.getInputStream()));

                    String line;
                    final StringBuilder log = new StringBuilder();
                    String separator = System.getProperty("line.separator");

                    while ((line = reader.readLine()) != null)
                    {
                        log.append(line);
                        log.append(separator);
                    }
                    String w = log.toString();
                    Log.d(TAG, "onStartJob: "+w);
                    Toast.makeText(getApplicationContext(), w, Toast.LENGTH_LONG).show();
                }
                catch (Exception e)
                {
                    Log.d(TAG, "onStartJob: "+e);
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }*/
            }
        }).start();


        //Intent i = new Intent(getApplicationContext(), PinActivity.class);
        //startActivity(i);
        jobFinished(params, true);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "onStopJob: Job cancelled before completion");
        jobCancelled = true;
        return false;
    }

    protected static void LockSettings() {
        SETTINGS_LOCKED = true;
    }

    protected static void UnlockSettings() {
        SETTINGS_LOCKED = false;
    }

    protected static boolean IsSettingsLocked() {
        return SETTINGS_LOCKED;
    }

    private static boolean SETTINGS_LOCKED = false;
    private static final String SETTINGS_STRING = "com.android.settings";


    protected static void LockPhone() {
        PHONE_LOCKED = true;
    }

    protected static void UnlockPhone() {
        PHONE_LOCKED = false;
    }

    protected static boolean IsPhoneLocked() {
        return PHONE_LOCKED;
    }

    private static boolean PHONE_LOCKED = false;
    private static boolean DEVICE_SLEEPING = false;

    protected static void saveSettings(ContextWrapper context) {
        FileManager.saveAppLockSettings(context, new String[]{String.valueOf(SETTINGS_LOCKED), String.valueOf(PHONE_LOCKED)});
    }
}
