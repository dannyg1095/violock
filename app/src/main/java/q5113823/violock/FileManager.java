package q5113823.violock;

import android.content.ContextWrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Daniel on 02/07/2020.
 */

final class FileManager {

    private final static String FILE_NAME = "data.txt";
    private final static String FILE_NAME1 = "data1.txt";
    private final static String FILE_NAME2 = "data2.txt";

    protected static String[] openFile(ContextWrapper context) {
        FileInputStream fis = null;

        String[] result = new String[2];
        try {
            fis = context.openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);

            result[0] = br.readLine();
            result[1] = br.readLine();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return result;
        }
    }

    protected static void saveFile(ContextWrapper context, String auth, String string) {
        FileOutputStream fos = null;

        try {
            fos = context.openFileOutput(FILE_NAME, MODE_PRIVATE);
            auth += "\n"+string;
            fos.write(auth.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected static void saveLockedApps(ContextWrapper context, ArrayList<String> apps) {
        FileOutputStream fos = null;

        File file = new File(context.getFilesDir(), FILE_NAME1);

        try {
            if(file.exists()) file.delete();
            file.createNewFile();

            fos = context.openFileOutput(FILE_NAME1, MODE_PRIVATE);
            for (String string : apps) {
                String newString = string + "\n";
                fos.write(newString.getBytes());
            }

            if (fos != null) fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static ArrayList<String> getLockedApps(ContextWrapper context) {
        FileInputStream fis = null;

        File file = new File(context.getFilesDir(), FILE_NAME1);
        ArrayList<String> apps = new ArrayList<>();
        try {
            if (file.exists()) {
                String app;

                fis = context.openFileInput(FILE_NAME1);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);

                app = br.readLine().toString();
                do {
                    apps.add(app);
                    app = br.readLine().toString();
                } while (app != null || !app.equals(""));
            } else {
                file.createNewFile();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return apps;
        }
    }

    protected static void saveAppLockSettings(ContextWrapper context, String[] settings) {
        FileOutputStream fos = null;

        File file = new File(context.getFilesDir(), FILE_NAME2);

        try {
            if(file.exists()) file.delete();
            file.createNewFile();

            fos = context.openFileOutput(FILE_NAME2, MODE_PRIVATE);
            for (String string : settings) {
                String newString = string + "\n";
                fos.write(newString.getBytes());
            }

            if (fos != null) fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static String[] getAppLockSettings(ContextWrapper context) {
        FileInputStream fis = null;

        File file = new File(context.getFilesDir(), FILE_NAME2);
        String[] settings = new String[]{"", ""};
        try {
            if (file.exists()) {
                fis = context.openFileInput(FILE_NAME2);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);

                settings[0] = br.readLine().toString();
                settings[1] = br.readLine().toString();
            } else {
                file.createNewFile();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return settings;
        }
    }
}
