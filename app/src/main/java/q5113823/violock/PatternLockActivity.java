package q5113823.violock;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.goodiebag.pinview.Pinview;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class PatternLockActivity extends AppCompatActivity {
    private IntruderImage intruderImage;
    private PatternLockView mPatternLock;
    private int retries = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_lock);

        intruderImage = new IntruderImage(getApplicationContext());
        intruderImage.openCamera();

        Bundle bundle = getIntent().getExtras();
        final Class previousActivity = bundle != null ? (Class) bundle.get("previousActivity") : null;
        final boolean toMain = bundle != null ? (boolean) bundle.get("toMain") : false;
        final String[] savedPattern = openFile();

        if(!savedPattern[0].equals("pattern")) {
            finish(); // no point in continuing if there isn't even a pattern
            return; // not sure if needed with "finish()"?
        }

        mPatternLock = findViewById(R.id.patternLock);
        mPatternLock.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {
            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                if (pattern.toString().equals(savedPattern[1])) {
                    //Intent i = new Intent(getBaseContext(), MainActivity.class); //Define new intent here
                    Intent i;
                    //i.putExtra("pin", pin);
                    //setResult(RESULT_OK, i);
                    //startActivity(i); //Start new activity here
                    if(previousActivity != null) {
                        i = new Intent(getBaseContext(), previousActivity);
                        Toast.makeText(PatternLockActivity.this, "to previousActivity", Toast.LENGTH_SHORT);
                        Log.d("PATTERN LOCK", "previous activity");
                        setResult(RESULT_OK, i);
                        startActivity(i);
                    } else if(toMain) {
                        Toast.makeText(PatternLockActivity.this, "to main activity", Toast.LENGTH_SHORT);
                        Log.d("PATTERN LOCK", "to main");
                        i = new Intent(getBaseContext(), MainActivity.class); //Define new intent here
                        setResult(RESULT_OK, i);
                        startActivity(i);
                    } else {
                        moveTaskToBack(true);
                    }
                    finish();
                } else {
                    retries--;
                    pattern.clear();
                    if(retries > 0) {
                        Toast.makeText(PatternLockActivity.this, "Retries Left: "+retries, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PatternLockActivity.this, "TOO MANY RETRIES", Toast.LENGTH_SHORT).show();
                        intruderImage.takePicture();
                    }
                }
            }

            @Override
            public void onCleared() {

            }
        });
    }

    @Override
    protected void onDestroy() {
        intruderImage.closeCamera();

        super.onDestroy();
    }

    private String[] openFile() {
        return FileManager.openFile(this);
    }
}
