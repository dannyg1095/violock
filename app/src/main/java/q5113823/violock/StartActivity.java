package q5113823.violock;

import android.Manifest;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class StartActivity extends AppCompatActivity {
    private static final String FILE_NAME = "data.txt";
    private static final String FILE_NAME1 = "data1.txt";
    private static final int VALIDATE_PIN = 1;
    private static final int CREATE_PIN = 2;

    private Intent passIntent;
    private Intent chooseAuthIntent;

    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        passIntent = new Intent(getBaseContext(), PinActivity.class);
        chooseAuthIntent = new Intent(getBaseContext(), ChooseAuthenticationActivity.class);

        file = new File(getFilesDir(), FILE_NAME);

        try {
            String[] auth = FileManager.openFile(this);

            if(auth[0] == null) { //if file doesn't exist
                file.createNewFile();
                Toast.makeText(StartActivity.this, "FILE NOT FOUND", Toast.LENGTH_LONG).show();
                startActivityForResult(chooseAuthIntent, 2);
                finish();
            } else if(auth[0].equals("pin")) {
                passIntent = new Intent(getBaseContext(), PinActivity.class);
                passIntent.putExtra("toMain", true);
                startActivity(passIntent);
            } else if(auth[0].equals("password")) {
                passIntent = new Intent(getBaseContext(), PasswordActivity.class);
                passIntent.putExtra("toMain", true);
                startActivity(passIntent);
            } else if(auth[0].equals("pattern")) {
                passIntent = new Intent(getBaseContext(), PatternLockActivity.class);
                passIntent.putExtra("toMain", true);
                startActivity(passIntent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get PACKAGE_USAGE_STATS perms so the app can see when an app is opened/launched
        //also CAMERA perms
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.PACKAGE_USAGE_STATS)) {
            Toast.makeText(this, "Please grant this app permission to use package usage stats", Toast.LENGTH_LONG);
            Intent i = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            startActivity(i);
        }*/

        //if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
        this.requestPermissions(new String[] {Manifest.permission.CAMERA, Manifest.permission.PACKAGE_USAGE_STATS, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        //}

        finish(); //needed to prevent skipping access control
    }

    //Wont get called because of "finish()" above
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if(requestCode == VALIDATE_PIN) {
                //String result = data.getStringExtra("code");


                //if (result.equals(openFile())) {

                //}
            } else if(requestCode == CREATE_PIN) {
                //String result = data.getStringExtra("code");
                //saveFile(result);

                //Toast.makeText(StartActivity.this, result + "", Toast.LENGTH_SHORT).show();
                //passIntent.putExtra("pin", result);
                //startActivityForResult(passIntent, 1);
            }
        }
    }
}
