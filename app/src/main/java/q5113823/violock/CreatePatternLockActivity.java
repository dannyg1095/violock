package q5113823.violock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class CreatePatternLockActivity extends AppCompatActivity {

    private TextView mTextView;
    private PatternLockView mPatternLockView;
    private int iterate = 1;
    private List<PatternLockView.Dot> firstPattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_lock);

        mTextView = findViewById(R.id.textView);
        mPatternLockView = findViewById(R.id.patternLock);
        mPatternLockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {
                // not needed
            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {
                // not needed
            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                if(iterate == 1) {
                    firstPattern = mPatternLockView.getPattern();
                    mPatternLockView.clearPattern();
                    mTextView.setText("Confirm PIN");
                    iterate--;
                } else if(iterate == 0) {
                    //if(firstPin.equals(mPinView.getValue())) {
                    if(firstPattern.equals(mPatternLockView.getPattern())) {
                        Intent i = new Intent(getBaseContext(), PatternLockActivity.class);
                        i.putExtra("toMain", true);
                        //i.putExtra("pin", firstPin);
                        saveFile("pattern" , firstPattern);
                        setResult(RESULT_OK);
                        startActivityForResult(i, 1);
                        finish();
                    } else {
                        Toast.makeText(CreatePatternLockActivity.this, "pattern mismatch. Try again.", Toast.LENGTH_SHORT).show();
                        mTextView.setText("Enter a New Pattern Lock");
                        mPatternLockView.clearPattern();
                        iterate++;
                    }
                }
            }

            @Override
            public void onCleared() {
                // not needed
            }
        });
    }

    private void saveFile(String auth, List<PatternLockView.Dot> pattern) {
        FileManager.saveFile(this, auth, pattern.toString());
    }
}
